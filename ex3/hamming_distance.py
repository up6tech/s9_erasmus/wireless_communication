def hamming_distance(iterable1, iterable2):
	dist_counter = 0
	for n in range(len(iterable1)):
		if iterable1[n] != iterable2[n]:
			dist_counter += 1
	return dist_counter

def xor(ci: list[int], cj: list[int]):
  cw = [0]*len(ci)
  for n in range(len(ci)):
    cw[n] = ci[n] ^ cj[n]
  return cw

def W(iterable: list[int]):
  sum_of_1 = 0
  for i in iterable:
    if i == 1:
      sum_of_1+=1
  return sum_of_1

def ex3_1_a():
  c = [[1,0,1],[1,1,1],[0,1,1]]
  min_hamming_distance = 99999999999999
  
  for i in range(len(c)):
    for j in range(len(c)):
      if c[i] is not c[j]:
        hamming_dist = W(xor(c[i], c[j]))
        if min_hamming_distance > hamming_dist:
          min_hamming_distance = hamming_dist
          
  print("a) Hamming distance = ",min_hamming_distance)

def ex3_1_b():
  c = [[0,0,0,0],[1,0,0,1],[0,1,1,0],[1,1,1,1]]
  min_hamming_distance = 99999999999999
  
  for i in range(len(c)):
    for j in range(len(c)):
      if c[i] is not c[j]:
        hamming_dist = W(xor(c[i], c[j]))
        if min_hamming_distance > hamming_dist:
          min_hamming_distance = hamming_dist
  print("b) Hamming distance = ",min_hamming_distance)

def ex3_1_c():
  c = [[0,0,0,0,0,0],[1,0,0,1,0,1],[0,1,1,0,1,0],[1,1,1,1,1,1]]
  min_hamming_distance = 99999999999999
  
  for i in range(len(c)):
    for j in range(len(c)):
      if c[i] is not c[j]:
        hamming_dist = W(xor(c[i], c[j]))
        if min_hamming_distance > hamming_dist:
          min_hamming_distance = hamming_dist
  print("c) Hamming distance = ",min_hamming_distance)


ex3_1_a()
ex3_1_b()
ex3_1_c()