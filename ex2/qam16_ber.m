function [BER, SNR] = qam16_ber(sourcelen, minSNR, maxSNR, incSNR)

  SNR = [minSNR:incSNR:maxSNR]
  numSNR = length(SNR)
  BER = zeros(1, numSNR)

  for i = 1:numSNR
    fprintf("Simulating SNR %f\n", SNR(i));
    BER(i) = qam16_src(sourcelen, SNR(i));
  end

  fprintf("Simulations complete!\n\n");

  [SNR' BER'];
  semilogy(SNR,BER);
  grid on;
  xlabel("SNR (dB)");
  ylabel("BER");

end

