function [BER] = qpsk_scr(bitsCount, snrDb)
    
    %  1    2    3     4
    %  0    1    3     2
    % 00   01   11    10
    c = [-1-1i -1+1i 1+1i 1-1i];
    seq_bits = round(rand(1, 2*bitsCount));
    input_2 = reshape(seq_bits, [2, length(seq_bits)/2]);
    
    dibits = bit2int(input_2, 2);
    seq_index = dibits+1;
    
    qpsk_stream = reshape(c(seq_index), [bitsCount, 1]);
    rxSig = awgn(qpsk_stream, snrDb, "measured");
    
    %h = scatterplot(rxSig);
    %hold on
    %scatterplot(c,[],[],'r*',h)
    %grid
    %hold off
    
    outputSig = [];
    % Get the closest point on the QPSK constellation
    for i=1:1:length(rxSig)
        z = rxSig(i);
        minimumDistance = 99999;
        index = -1;
        for y=1:1:length(c)
            d = abs(z-c(1,y));
            if minimumDistance > d
                minimumDistance = d;
                index = y;
            end
        end
        b = int2bit(index-1, 2);
        outputSig(end+1) = b(1);<w
        outputSig(end+1) = b(2); 
    end

    missed = double(0);

    for i=1:1:length(outputSig)
        original = seq_bits(i);
        output = outputSig(i);
        if original ~= output
            missed = missed+1;
        end
    end

    BER = missed/double(length(outputSig))
end
