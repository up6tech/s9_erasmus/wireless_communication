function [BER] = qam16_src(bitsCount, snrDb)
    
  %   0     1     2    3
  % 0000  0001  0010  0011
  %   4     5     6    7
  % 0100  0101  0110  0111
  %   8     9    10    11
  % 1000  1001  1010  1011
  %  12    13    14    15   
  % 1100  1101  1110  1111


  % to change
  c = [-3-3i -3-1i -3+3i -3+1i -1-3i -1-1i -1+3i -1+1i 3-3i  3-1i  3+3i  3+1i 1-3i  1-1i  1+3i 1+1i];


  seq_bits = round(rand(1, 4*bitsCount));
  input_2 = reshape(seq_bits, [4, length(seq_bits)/4]);
  
  dibits = bit2int(input_2, 4);
  seq_index = dibits+1;
  
  qpsk_stream = reshape(c(seq_index), [bitsCount, 1]);
  rxSig = awgn(qpsk_stream, snrDb, "measured");
  
  %h = scatterplot(rxSig);
  %hold on
  %scatterplot(c,[],[],'r*',h)
  %grid
  %hold off
  
  outputSig = [];
  % Get the closest point on the QPSK constellation
  for i=1:1:length(rxSig)
      z = rxSig(i);
      minimumDistance = 99999;
      index = -1;
      for y=1:1:length(c)
          d = abs(z-c(1,y));
          if minimumDistance > d
              minimumDistance = d;
              index = y;
          end
      end
      b = int2bit(index-1, 4);
      outputSig(end+1) = b(1);
      outputSig(end+1) = b(2); 
      outputSig(end+1) = b(3); 
      outputSig(end+1) = b(4); 
  end

  missed = double(0);

  for i=1:1:length(outputSig)
      original = seq_bits(i);
      output = outputSig(i);
      if original ~= output
          missed = missed+1;
      end
  end

  BER = missed/double(length(outputSig))
end
